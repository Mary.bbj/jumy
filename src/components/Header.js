import React, { useEffect, useState } from 'react'

import logo from '../assets/images/logo.png'
import search from '../assets/images/search.png'
import hamburger from '../assets/images/open.png'
import close from '../assets/images/close.png'

function Header() {
    const [open, setOpen] = useState(false)
    useEffect(() => {
        document.addEventListener(
            'section-changed',
            () => {
                setOpen(false)
            },
            false
        )
    })

    return (
        <header className={`${open ? 'open-menu' : ''}`}>
            <div className="open-close-menu show-mob">
                <img src={hamburger} alt="icon" className="open" onClick={() => setOpen(true)} />
                <img src={close} alt="icon" className="close" onClick={() => setOpen(false)} />
            </div>
            <nav>
                <a href="/" className="logo">
                    <img src={logo} alt="icon" />
                </a>
                <div className="search">
                    <img src={search} alt="icon" />
                    <input type="text" placeholder="Search art or artists..." />
                </div>
                <div className="menu-items">
                    <ul>
                        <li> <a href="/" className="active">  Explore </a> </li>
                        <li> <a href="/">Drops</a> </li>
                        <li> <a href="/">Community</a> </li>
                        <li> <button>Connect Wallet</button> </li>
                    </ul>
                </div>
            </nav>
        </header>
    )
}

export default Header
