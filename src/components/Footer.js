import arrowUp from '../assets/images/arrow-up.png'

function Footer() {
    return (
        <footer>
            <div className="artist">
                <h1>Are you an artist?</h1>
                <button>Tell us about you</button>
            </div>
            <div className="social-info">
                <p>
                    The Offices 4, One Central
                    <br /> Dubai, United Arab Emirates
                </p>
                <a href="mailto:hello@jumy.co" className="email">
                    hello@jumy.co
                </a>

                <ul className="socials">
                    <li> <a href="/">Instagram</a> </li>
                    <li> <a href="/">Twitter</a> </li>
                    <li> <a href="/">Medium</a> </li>
                </ul>
            </div>
            <div className="info">
                <p>All right reserved.</p>
                <img src={arrowUp} alt="" className="arrow-up" />
                <ul>
                    <li> <a href="/">FAQ </a>  </li>
                    <li> <a href="/"> Terms</a>  </li>
                    <li> <a href="/"> Policy</a> </li>
                </ul>
            </div>
        </footer>
    )
}

export default Footer
