import Product from './pages/product'

import Header from './components/Header'
import Footer from './components/Footer'

import './assets/index.scss'

function Jumy() {
    return (
        <div className="wrapper">
            <Header />
            <Product />
            <Footer />
        </div>
    )
}

export default Jumy
