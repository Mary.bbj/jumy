import artwork from '../assets/images/Artwork.jpg'
import like from '../assets/images/like.png'
import share from '../assets/images/share.png'
import more from '../assets/images/more.png'
import logo2 from '../assets/images/logo2.jpg'
import vector from '../assets/images/vector.png'
import avatar from '../assets/images/avatar.png'
import verified from '../assets/images/verified.png'
import eth from '../assets/images/eth.png'
import artwork1 from '../assets/images/artwork1.jpg'
import artwork2 from '../assets/images/artwork2.jpg'
import artwork3 from '../assets/images/artwork3.jpg'
import insta from '../assets/images/insta.png'
import twitter from '../assets/images/twitter.png'

function Product() {
    return (
        <div className="pdp-wrapper">
            <h2 className="title">Explore Drops</h2>
            <div className="tabs">
                <button className="active"> Live</button>
                <button>Upcoming</button>
                <button>Past</button>
            </div>
            <div className="content">
                <div className="left-side">
                    <img src={artwork} alt="" className="artwork" />
                    <div className="about-artist">
                        <div className="artist-social">
                            <div className="artist-intro">
                                <div>
                                    <div className="verified-img">
                                        <img src={avatar} alt="" />
                                        <img src={verified} alt="" />
                                    </div>
                                    <div className="artist-name">
                                        <p>Paul Vihez</p>
                                        <p>@Jumaker</p>
                                    </div>
                                </div>
                                <p>Jumaker is a contemporary digital Artist with a unique signature and very authentic genr... </p>
                            </div>
                            <div className="images">
                                <img src={artwork1} alt="img" />
                                <img src={artwork2} alt="img" />
                                <img src={artwork3} alt="img" />
                                <div className="more">
                                    <input type="file" name="" id="" />
                                    <p>More +</p>
                                </div>
                            </div>
                            <div className="socials">
                                <div>
                                    <div>
                                        <img src={insta} alt="icon" />
                                        <a href="/"> @Jumaker</a>
                                    </div>
                                    <div>
                                        <img src={twitter} alt="icon" />
                                        <a href="/"> @Jumaker</a>
                                    </div>
                                </div>

                                <div>
                                    <button>Follow</button>
                                    <img src={vector} alt="icon" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="right-side">
                    <div className="header">
                        <p> Edition 4/4</p>
                        <div>
                            <img src={like} alt="icon" />
                            <img src={share} alt="icon" />
                            <img src={more} alt="icon" />
                        </div>
                    </div>
                    <h2 className="art-title">Idyllic lake</h2>
                    <div className="description">
                        <p>
                            Idyllic Lake is a forward-thinking collection of digital artworks that mixes surrealism and impressionism. There are plenty of subliminal
                            messages embedded in every single artworks that makes it contemplative and very intriguing. This collection features a unique artwork.
                        </p>
                        <img src={logo2} alt="icon" />
                    </div>
                    <div className="money-time">
                        <div className="money">
                            <p className="black-bg">CURRENT BID</p>
                            <div>
                                <img src={eth} alt="" /> 
                                <p>3.50 ETH</p>
                            </div>
                            <span>$ 10.000 USD</span>
                        </div>
                        <div className="time">
                            <p className="black-bg">ENDS IN</p>
                            <div>
                                <p>19</p> <span>hours</span>
                            </div>
                            <div>
                                <p>26</p> <span>minutes</span>
                            </div>
                            <div>
                                <p>32</p> <span>seconds</span>
                            </div>
                        </div>
                    </div>
                    <div className="buyign">
                        <button>Buy with ETH</button>
                        <button>Buy with Card</button>
                    </div>
                    <div className="artist-info">
                        <div className="tabs">
                            <button className="active"> Owners</button>
                            <button>Bids</button>
                            <button>History</button>
                        </div>
                        <div className="art-content">
                            <div className="owner">
                                <div>
                                    <div className="verified-img">
                                        <img src={avatar} alt="icon" />
                                        <img src={verified} alt="icon" />
                                    </div>
                                    <div>
                                        <p> <span>@jumaker</span> owned this NFT  </p>
                                        <p> 1/4 sale for <span>0.01 ETH</span> each  </p>
                                    </div>
                                </div>
                                <a href="/">
                                    <img src={vector} alt="icon" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product
